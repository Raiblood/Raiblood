

// carga de librerías adicionales:
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql      = require('mysql');
var fs = require('fs');
var port = 3000;
var config = {
  host: "192.168.100.71",
  user: "root",
  password: "a",
  database: "test"
};

connection = mysql.createConnection(config);
connection.connect();




// configuración de /public y bodyParser
app.use( bodyParser.json() );  
app.use( bodyParser.urlencoded({ extended: true }) );  
app.use( express.static('public') );


app.post('/nuevo', (req, res) => {
 let sql ='insert into alumnos (nombre,apellidos) values (?,?);'
 let valores = [req.body.nombre, req.body.apellidos] 
 connection.query(sql,valores, (err, results) => {
    // console.log(results)
    res.json(results)
 })
})



// ruta BASE
// GET /
app.get('/', function (req, res) {
  res.sendFile( __dirname + '/forms/index.html');  
});

// listado JSON de alumnos con SELECT
// GET /alumnos
app.get('/alumnos', function (req, res) {      
  // TODO: pasarle ?orderby=campo
  connection.query('SELECT * FROM alumnos;', function (error, results, fields) {        
    res.json(results);
  });  
});

// ejemplo de llamada a un procedimiento de MySql;
// GET /getAlumnos
app.get('/getAlumnos/', function(req,res){  
  query = `call getAlumnos();`
  connection.query(query, function(error, results){    
    res.json(results[0]);
  })
});

// obtiene de MySql el alumno con id = :id
// GET /alumno/:id
app.get('/alumno/:id', (req,res) => {
  let sql = 'select * from alumnos where id = ?' 
  console.log(req.params.id)

  connection.query(sql, [req.params.id], function (error, results) {     
    // si no se encuentra el registro se devuelve objeto blanco mejor que pantalla blanca
    res.json( results[0] ? results[0] : {} )
  });    
})

// grabación de alumnos vía html FORM 
// a) los datos se envían con método POST en el cuerpo del request
// b) por convención, el INSERT o UPDATE se decide por el valor del
// campo id del FORM; puesto que es un campo hidden, sólo puede tener
// valor si se está editando y se hace UPDATE; si no tiene valor se hace INSERT
// POST /alumno --data {...}
app.post('/alumno', (req,res) => {     
  let alumno = req.body, sql = '' ;
  if ( parseInt(alumno.id) > 0 ){    
    sql = 'update alumnos set apellidos = ? , nombre = ? where id = ?';
    connection.query(sql, [alumno.apellidos, alumno.nombre, alumno.id], function (error, results, fields) {      
    });    
  } else {
    sql = 'insert into alumnos (apellidos,nombre) values (?, ?)';
    connection.query(sql, [alumno.apellidos, alumno.nombre], function (error, results, fields) {
    });   
  }
  res.redirect('/index.html');
});

// borrado de alumnos:
// desde el cliente se manda un Ajax de type DELETE
// DELETE /alumno/:id
app.delete('/alumno/:id', (req,res) => {       
  let sql = 'delete from alumnos where id = ?'
  connection.query(sql, [req.params.id], function (error, results, fields) {
    res.json(results)
  });
});

// envío de archivos que no están en la carpeta de static
// por ejemplo formularios de edición, otros archivos de la app
// o cualquier archivo cuya url queramos no mostrar;
app.get('/newalumno', (req, res) => {
  res.sendFile( __dirname + '/new-alumno.html');  
});

// arrancar el server
app.listen(port, function(){
  console.log('mysql middleWare escuchando en puerto ' + port);
});

