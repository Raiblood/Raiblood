var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var mysql      = require('mysql')
var connection = mysql.createConnection({
  host     : '192.168.100.71',
  database : 'test',
  user     : 'root',
  password : 'a'
})
var port = 3333
connection.connect();
app.use(require('body-parser').json());    
app.use( bodyParser.urlencoded({extended: true}))
app.use( express.static('public'))

app.get('/', function (req, res) {
  let msg = `
    <h2>middleWare Express - Mysql</h2>
    <h3>uso</h3>
    <li>GET http://192.168.100.71:/alumnos</li>
  `
  res.send(msg);
});

app.get('/alumnos', function (req, res) {    
  
  connection.query('SELECT * FROM alumnos;', function (error, results, fields) {    
    res.send(results)
  });  
});

app.get('/alumno/:id', (req,res) => {
  let sql = `
    select * from alumnos 
    where id=${req.params.id}
  `
  connection.query(sql, function (error, results, fields) {        
    res.send(results[0])
  });    
})

app.post('/alumno', (req,res) => {      
  let alumno = req.body
  let sql = `update alumnos set apellidos = ?, nombre = ? , id_profesor = ? where id = ?`
  console.log(req.body)
  connection.query(sql, [alumno.apellidos, alumno.nombre, alumno.id_profesor, alumno.id], function (error, results, fields) {        
    console.log(results)    
    res.redirect('/')
  });
});

app.delete('alumno/:id ',(req,res) => {
  let sql ='delete from alumnos where id = ?'
  connection.query(sql,[req.params.id], function(error,results,fields) {
    res.json(results)
  });
});

app.listen(port, function () {
  console.log('mysql middleWare escuchando en puerto ' + port);
});


